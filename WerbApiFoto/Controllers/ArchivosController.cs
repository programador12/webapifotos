﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WerbApiFoto.Services;

namespace WerbApiFoto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArchivosController : ControllerBase
    {
        private IArchivoService archivoService;
        public ArchivosController(IArchivoService archivoService)
        {
            this.archivoService = archivoService;
        }

        [HttpPost("Cargar")]
        public async Task<IActionResult> Cargar(IFormFile file)
        {
            try
            {
                string files = await archivoService.GuardarArchivo(file);
                if (!files.Contains("ERROR"))
                    return Ok(
                         new
                         {
                             archivo = files
                         });
                else return BadRequest(files + "Hola6");
            }
            catch (System.Exception ex) { return BadRequest(ex.Message + "Hola5"); }
        }
        //[FromForm(Name = "file")] IFormFile file
    }
}
