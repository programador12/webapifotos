﻿using System;
using System.IO;
using System.Threading.Tasks;
using WerbApiFoto.Helpers;

namespace WerbApiFoto.Repository
{
    public class ADFoto
    {
        public async Task<string> _GuardarArchivo(FileStream file, string Nombre)
        {
            try
            {
                string respuesta = await StorageAmazon.AsyncCargarArchivo(file, Nombre);
                if (respuesta.Equals("OK")) { return Nombre; }
                else return respuesta;
            }
            catch (Exception ex) { return "ERROR:" + ex.Message + "Hola3"; }
        }
    }
}
