﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WerbApiFoto.Helpers;

namespace WerbApiFoto.Services
{
    public interface IArchivoService
    {
        Task<string> GuardarArchivo(IFormFile file);
    }
    public class ArchivoService : IArchivoService
    {
        
        public async Task<string> GuardarArchivo(IFormFile file)
        {
            string extension = "1";
            string nombre = "2";
            string respuesta = ":(";
            string x = "1";
            try
            {
                if (file != null)
                {
                    if (file.Length > 0)
                    {
                        x = "2";
                        FileStream fileStream = new FileStream(file.FileName, FileMode.Create);
                        file.CopyTo(fileStream);
                        extension = Path.GetExtension(file.FileName);
                        nombre = Guid.NewGuid().ToString() + extension;
                        respuesta = await _GuardarArchivo(fileStream, "FotosMovil/" + nombre);
                        if (respuesta.Equals("OK")) { return nombre; }
                        else return respuesta;
                    }
                    else { x = "3"; return "ERROR: NO RECIBO ARCHIVO"; }
                }
                else { x = "4"; return "ERROR: ARCHIVO NULL"; }
            }
            catch (Exception ex) { return "ERROR:" + ex.Message + "Hola4 ext:" + extension + " nom: " + nombre + " resp: " + respuesta + " x =" + x; }
        }
        public async Task<string> _GuardarArchivo(FileStream file, string Nombre)
        {
            try
            {
                string respuesta = await StorageAmazon.AsyncCargarArchivo(file, Nombre);
                if (respuesta.Equals("OK")) { return Nombre; }
                else return respuesta;
            }
            catch (Exception ex) { return "ERROR:" + ex.Message + "Hola3"; }
        }
    }
}
